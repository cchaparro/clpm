(in-package #:clpm-test)

(defvar *clpm* "clpm"
  "How to run clpm")

(defvar *clpm-env* nil)

(defun clpm (command &key output error-output)
  (apply 'uiop:run-program
         (list* *clpm* command)
         :output output :error-output error-output
         (run-program-augment-env-args *clpm-env*)))

(defun call-with-clpm-env (thunk &key config-dir)
  (with-temporary-directory (cache-dir :prefix "clpm-test-cache")
    (with-temporary-directory (data-dir :prefix "clpm-test-data")
      (let ((*clpm-env* (list (cons "CLPM_DATA_DIR" (uiop:native-namestring data-dir))
                              (cons "CLPM_CACHE_DIR" (uiop:native-namestring cache-dir))
                              (cons "CLPM_GROVEL_SANDBOX_TYPE" "nil"))))
        (when (keywordp config-dir)
          (push (cons "CLPM_CONFIG_DIRS" (uiop:native-namestring
                                          (asdf:system-relative-pathname
                                           :clpm
                                           (concatenate 'string "test/configs/"
                                                        (string-downcase config-dir)
                                                        "/"))))
                *clpm-env*))
        ;; TODO: remove this. Instead of modifying the environment of this
        ;; process, we should add a set of arguments that let us set any config
        ;; option via the CLI and teach clpm-client how to use that.
        (let ((orig-data-dir (or (uiop:getenvp "CLPM_DATA_DIR") ""))
              (orig-cache-dir (or (uiop:getenvp "CLPM_CACHE_DIR") ""))
              (orig-config-dirs (or (uiop:getenvp "CLPM_CONFIG_DIRS") ""))
              (orig-grovel-sandbox-type (or (uiop:getenvp "CLPM_GROVEL_SANDBOX_TYPE") "")))
          (unwind-protect
               (progn
                 (setf (uiop:getenv "CLPM_DATA_DIR") (uiop:native-namestring data-dir)
                       (uiop:getenv "CLPM_CACHE_DIR") (uiop:native-namestring cache-dir)
                       (uiop:getenv "CLPM_GROVEL_SANDBOX_TYPE") "nil")
                 (when (keywordp config-dir)
                   (setf (uiop:getenv "CLPM_CONFIG_DIRS")
                         (uiop:native-namestring
                          (asdf:system-relative-pathname
                           :clpm
                           (concatenate 'string "test/configs/"
                                        (string-downcase config-dir)
                                        "/")))))
                 (funcall thunk :cache-dir cache-dir :data-dir data-dir))
            (setf (uiop:getenv "CLPM_DATA_DIR") orig-data-dir
                  (uiop:getenv "CLPM_CACHE_DIR") orig-cache-dir
                  (uiop:getenv "CLPM_CONFIG_DIRS") orig-config-dirs
                  (uiop:getenv "CLPM_GROVEL_SANDBOX_TYPE") orig-grovel-sandbox-type)))))))

(defmacro with-clpm-env ((&key (cache-dir (gensym)) (data-dir (gensym)) config-dir)
                         &body body)
  `(call-with-clpm-env (lambda (&key ((:cache-dir ,cache-dir)) ((:data-dir ,data-dir)))
                         (declare (ignorable ,cache-dir ,data-dir))
                         ,@body)
                       :config-dir ,config-dir))
